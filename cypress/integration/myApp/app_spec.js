describe('My First Test', function() {
  it('Does not do much!', function() {
    expect(true).to.equal(true)
  })
});
describe('My Second Test', function() {
  it('Visits the Kitchen Sink', function() {
    cy.visit('https://example.cypress.io')
  })
});
describe('My Third Test', function() {
  it('finds the content "type"', function() {
    cy.visit('https://example.cypress.io');
    cy.contains('type');
    cy.contains('window').click();
  })
});
describe('My Fourth Test', function() {
  it('finds the content "window" and click on it', function() {
    cy.visit('https://example.cypress.io');
    cy.contains('window').click();
    // Should be on a new URL which includes '/commands/actions'
    cy.url().should('include', '/commands/window')
  })
});
describe('My Fifth Test', function() {
  it('Gets, types and asserts', function() {
    cy.visit('https://example.cypress.io')

    cy.contains('type').click()

    // Should be on a new URL which includes '/commands/actions'
    cy.url().should('include', '/commands/actions')

    // Get an input, type into it and verify that the value has been updated
    cy.get('.action-email')
      .type('fake@email.com')
      .should('have.value', 'fake@email.com');

       // Delay each keypress by 0.1 sec
      cy.get('#password1')
		  .type('slow.typing@email.com', { delay: 400 })
		  .should('have.value', 'slow.typing@email.com');
  })
});
describe('My Sixth Test', function() {
  it('clicking "type" shows the right headings', function() {
    cy.visit('https://example.cypress.io');
    // cy.pause();
    cy.contains('type').click();
    // Should be on a new URL which includes '/commands/actions'
    cy.url().should('include', '/commands/actions');
    // Get an input, type into it and verify that the value has been updated
    // cy.get('.action-email1')
    //   .type('fake@email.com')
    //   .should('have.value', 'fake@email.com');
    cy.get('.action-email');
  })
})
describe('My Test for response.status', function() {
  it('Check the status of response', function() {
  	cy.request({
  		url: 'https://example.cypress.io/fdsfdsfds',
  		followRedirect: false, // turn off following redirects  	
  		failOnStatusCode: false
	})
  .then((resp) => {
    // redirect status code is 302
    expect(resp.status).to.eq(404)
  })
})
})
// cy.visit('/abcde', {failOnStatusCode: false});
// cy.get('#email').type('my@email.com');
// cy.get('#submit').click();
// cy.url().should('contain','/form-success');